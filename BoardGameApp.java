import java.util.Random;
import java.util.Scanner;
public class BoardGameApp {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Random rng = new Random();
		Board board = new Board();
		
		//COLORS
		final String WHITE = "\u001B[0m";
		final String LAVENDER = "\u001b[38;5;147m";
		final String AQUA = "\u001b[38;2;145;231;255m";
		final String PINK = "\u001B[45m";
		final String RESET = "\033[0m";
		
		//into
		System.out.println(LAVENDER+ "howdy partner, lets play a game of " +AQUA+ "Castle" +RESET);
		
		int numCastles = 5;
		int turns = 0;
		
		while(numCastles>0 && turns<8){
			System.out.println(board);
			
			// castles/turns
			System.out.println(LAVENDER+ "here's the number of castles u got: " +AQUA+ numCastles +RESET);
			System.out.println(LAVENDER+ "here's the amount of turns u did: " +AQUA+ turns +RESET+ "\n");
			
			// row/column
			System.out.println("pls, input 2 numbers\n [pick a number between 1 and 5]");
			System.out.print("row: " +AQUA);
			int row = reader.nextInt() - 1;
			reader.nextLine();
			System.out.print(RESET);
			System.out.print("column: " +AQUA);
			int col = reader.nextInt() - 1;
			reader.nextLine();
			System.out.print(RESET+ "\n");
			
			//placeToken
			int placedToken = board.placeToken(row, col);
			while(placedToken<0){
				System.out.println("u cant place a castle there silly, try again\n [pick a number between 1 and 5]");
				System.out.print("row: " +AQUA);
				row = reader.nextInt() - 1;
				reader.nextLine();
				System.out.print(RESET);
				System.out.print("column: " +AQUA);
				col = reader.nextInt() - 1;
				reader.nextLine();
				System.out.print(RESET+ "\n");
				placedToken = board.placeToken(row, col);
			}
			if(placedToken==1){
				turns++;
				System.out.println("there was a hidden wall there.. u wasted a turn but iss okay, try again\n [pick a number between 1 and 5]");
				
			}
			if(placedToken==0){
				numCastles--;
				System.out.println("castle placed successfully, go again\n [pick a number between 1 and 5]");				
				
			}
		}
		System.out.println(board);
		if(numCastles!=0)
			System.out.println("sorry.. u lost");
		else
			System.out.println("congrats!! u won");
	}
}