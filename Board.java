//System.out.println("testin (question mark)");
import java.util.Random;
public class Board{
	private Tile[][] grid;
	final int size;
	
	//COLORS
	final String PINK = "\u001B[45m";
	final String RESET = "\033[0m";
	
	public Board(){
		Random rng = new Random();
		size = 5;
		this.grid = new Tile [size][size];
		for(int i=0; i<this.grid.length; i++){
			for(int x=0; x<this.grid.length; x++){
				grid[i][x] = Tile.BLANK;
			}
		}
		for(int i=0; i<this.grid.length; i++){
			int randomInt = rng.nextInt(this.grid.length);
			grid[i][randomInt] = Tile.HIDDEN_WALL;
		}
		
	}
	
	public String toString(){
		String board = "\n";
		for(int i=0; i<this.grid.length; i++){
			board += "	"+PINK;
			for(int x=0; x<this.grid.length; x++){
				board+= this.grid[i][x].getName() + " ";
			}
			board+= RESET+"\n";
		}
		return board;
	}
	
	public int placeToken(int row, int col){
		if(row>=size || col>=size || row<0 || col<0){
			return -2;
		}
		else if(this.grid[row][col].equals(Tile.WALL) || this.grid[row][col].equals(Tile.CASTLE)){
			return -1;
		}
		else if(this.grid[row][col].equals(Tile.HIDDEN_WALL)){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		this.grid[row][col] = Tile.CASTLE;
		return 0;
	}
}